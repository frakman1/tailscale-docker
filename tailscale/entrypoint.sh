#!/bin/sh
set -x

# Setup iptables
iptables -t nat -A POSTROUTING -o tailscale0 -j MASQUERADE

if [ ! -d /dev/net ]; then mkdir /dev/net; fi
if [ ! -e /dev/net/tun ]; then  mknod /dev/net/tun c 10 200; fi

# Start the daemon
/usr/bin/tailscaled --state=/tailscale/tailscaled.state &

# Let it get connected to the control plane
sleep 10

# Start the interface
/usr/bin/tailscale up --accept-routes --advertise-exit-node --advertise-routes ${ROUTES} --login-server ${LOGINSERVER} --authkey ${AUTHKEY} &

# Start downloads if enabled
if [ "${ENABLE_DOWNLOADS}" = "TRUE" ]; then
    echo "Enabling downloads"
    /usr/bin/tailscale file get -wait=true -conflict=rename -loop=true /downloads &
fi

sleep infinity